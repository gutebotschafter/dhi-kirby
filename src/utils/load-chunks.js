export default () => {
    switch (window.__CHUNK__) {
        case 'dummy-script':
            import ('../dummy-script' /* webpackChunkName: "dummy-script" */).then(dummyScript => {
                dummyScript.init();
            });
            break;
        case 'other-script':
            import('../other-script' /* webpackChunkName: "other-script" */);
    }
};
