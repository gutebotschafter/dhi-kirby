<?php

return [
    'debug'  => false,
    'locale' => 'de_DE.utf-8',
    'smartypants' => true,
    'cache' => [
      'pages' => [
          'active' => true,
          'ignore' => function ($page) {
            return $page->template() === 'home';
            return $page->template() === 'events';
          }
      ]
  ]
];
