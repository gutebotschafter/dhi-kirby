<?php

return [
    'debug'  => true,
    'cache'  => false,
    'locale' => 'de_DE.utf-8',
    'smartypants' => true
];
