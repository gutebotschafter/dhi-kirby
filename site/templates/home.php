<?php snippet('header') ?>

<main class="page-main" id="<?= $page->slug() ?>">

  <header class="title<?php if($site->coverImages()->isNotEmpty()): ?> title--image<?php endif ?>">
    <div class="title__headline">
      <?php if($site->claim()->isNotEmpty()): ?>
        <h1><?= $site->claim() ?></h1>
      <?php else: ?>
        <h1><?= $site->title() ?></h1>
      <?php endif ?>
    </div>
    <?php if($page->coverImages()->toFiles()->count() > 0): ?>
      <div class="title__images title__images--setof<?= $page->coverImages()->toFiles()->count() ?>">
        <?php foreach($page->coverImages()->toFiles() as $image): ?>
          <figure>
            <img src="<?= $image->thumb(['width' => 1280, 'height' => 640, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            <?php if($image->cite()->isNotEmpty()): ?>
              <cite><?= $image->cite()->html() ?></cite>
            <?php endif ?>
          </figure>
        <?php endforeach ?>
      </div>
    <?php endif ?>
  </header>

  <section class="page-main__content">

    <?php if($page->text()->isNotEmpty()): ?>
      <div class="text">
        <div class="text__main">
          <?= $page->text()->kirbytext() ?>
        </div>
      </div>
    <?php endif ?>

    <header class="headline">
      <div class="headline__icon">
        <?php snippet('svg/icons/bell') ?>
      </div>
      <div class="headline__text">
        <h2><?= $pageNews->title() ?></h2>
      </div>
    </header>

    <ul class="list-news">

      <?php foreach($kirby->collection('news')->limit(1) as $item): ?>
        <li class="list-news__item">
          <h3><a href="<?= $item->url() ?>" title="Diesen Eintrag anzeigen"><?= $item->title() ?></a></h3>
          <?php if($image = $item->coverImages()->toFiles()->first()): ?>
            <figure class="list-news__item__image">
              <img src="<?= $image->thumb(['width' => 240, 'height' => 160, 'crop' => true, 'grayscale' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            </figure>
          <?php endif ?>
          <?php if($item->excerpt()->isNotEmpty()): ?>
            <p><?= $item->excerpt()->html() ?></p>
          <?php else: ?>
            <p><?= $item->text()->excerpt('255', 'true', '…') ?></p>
          <?php endif ?>
          <div class="list-news__item__meta">
            <time>
              <?= $item->date()->toDate('d.m.Y') ?>
            </time>
          </div>
        </li>
      <?php endforeach ?>

      <?php if($page->featurePage()->isNotEmpty()): ?>
        <li class="list-news__feature">
          <div class="list-news__feature__text">
            <h3><a href="<?= $page->featurePage()->toPage()->url() ?>" title="Erfahren Sie mehr"><?= $page->featureTitle() ?></a></h3>
            <p><?= $page->featureText()->html() ?></p>
          </div>
          <figure class="list-news__feature__image">
            <img src="<?= $page->featureImage()->toFile()->thumb(['width' => 320, 'quality' => 90])->url() ?>" alt="<?= $page->featureImage()->toFile()->title() ?>">
          </figure>
        </li>
      <?php endif ?>

      <?php foreach($kirby->collection('news')->limit(3)->offset(1) as $item): ?>
        <li class="list-news__item">
          <h3><a href="<?= $item->url() ?>" title="Diesen Eintrag anzeigen"><?= $item->title() ?></a></h3>
          <?php if($image = $item->coverImages()->toFiles()->first()): ?>
            <figure class="list-news__item__image">
              <img src="<?= $image->thumb(['width' => 240, 'height' => 160, 'crop' => true, 'grayscale' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            </figure>
          <?php endif ?>
          <?php if($item->excerpt()->isNotEmpty()): ?>
            <p><?= $item->excerpt()->html() ?></p>
          <?php else: ?>
            <p><?= $item->text()->excerpt('255', 'true', '…') ?></p>
          <?php endif ?>
          <div class="list-news__item__meta">
            <time>
              <?= $item->date()->toDate('d.m.Y') ?>
            </time>
          </div>
        </li>
      <?php endforeach ?>

    </ul>

    <nav class="more">
      <a href="<?= $pageNews->url() ?>" title="Die Seite <?= $pageNews->title() ?>anzeigen">Alle Einträge anzeigen</a>
    </nav>

  </section>

  <aside class="page-main__content page-main__content--aside">

    <header class="headline">
      <div class="headline__icon">
        <?php snippet('svg/icons/bell') ?>
      </div>
      <div class="headline__text">
        <h2><?= $pageEvents->title() ?></h2>
      </div>
    </header>

    <ul class="list-events">
      <?php foreach($kirby->collection('events')->limit(4) as $item): ?>
        <li class="list-events__item">
          <header>
            <h3>
            <time>
              <?php if($item->dateText()->isNotEmpty()): ?>
                <?= $item->dateText()->html() ?>
              <?php else: ?>
                <?= $item->dateStart()->toDate('d.m.Y') ?><?php e($item->dateEnd()->isNotEmpty(), ' – ' . $item->dateEnd()->toDate('d.m.Y')) ?>
              <?php endif ?>
            </time>
          </h3>
          </header>
          <?= $item->text()->kt() ?>
        </article>
      <?php endforeach ?>
    </ul>

    <nav class="more">
      <a href="<?= $pageEvents->url() ?>" title="Die Seite <?= $pageEvents->title() ?>anzeigen">Alle Einträge anzeigen</a>
    </nav>

  </aside>

</main>

<?php snippet('footer') ?>
