<?php snippet('header') ?>

<main class="page-main" id="<?= $page->slug() ?>">

  <header class="title<?php if($page->coverImages()->isNotEmpty()): ?> title--image<?php endif ?>">
    <div class="title__headline">
      <?php if($page->headline()->isNotEmpty()): ?>
        <h1><?= $page->headline() ?></h1>
      <?php else: ?>
        <h1><?= $page->title() ?></h1>
      <?php endif ?>
    </div>
    <?php $coverImages = $kirby->collection('news')->listed()->first()->coverImages()->toFiles(); ?>

    <?php if($page->coverImages()->toFiles()->count() > 0): ?>
      <div class="title__images title__images--setof<?= $page->coverImages()->toFiles()->count() ?>">
        <?php foreach($page->coverImages()->toFiles() as $image): ?>
          <figure>
            <img src="<?= $image->thumb(['width' => 1280, 'height' => 640, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            <?php if($image->cite()->isNotEmpty()): ?>
              <cite><?= $image->cite()->html() ?></cite>
            <?php endif ?>
          </figure>
        <?php endforeach ?>
      </div>
    <?php endif ?>
  </header>

  <section class="page-main__content">

    <ul class="list-news">

      <?php foreach($kirby->collection('news') as $item): ?>
        <li class="list-news__item">
          <h3><a href="<?= $item->url() ?>" title="Diesen Eintrag anzeigen"><?= $item->title() ?></a></h3>
          <?php if($image = $item->coverImages()->toFiles()->first()): ?>
            <figure class="list-news__item__image">
              <img src="<?= $image->thumb(['width' => 240, 'height' => 160, 'crop' => true, 'grayscale' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            </figure>
          <?php endif ?>
          <?php if($item->excerpt()->isNotEmpty()): ?>
            <p><?= $item->excerpt()->html() ?></p>
          <?php else: ?>
            <p><?= $item->text()->excerpt('255', 'true', '…') ?></p>
          <?php endif ?>
          <div class="list-news__item__meta">
            <time>
              <?= $item->date()->toDate('d.m.Y') ?>
            </time>
          </div>
        </li>
      <?php endforeach ?>

    </ul>

  </section>

</main>

<?php snippet('footer') ?>
