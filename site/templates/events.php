<?php snippet('header') ?>

<main class="page-main" id="<?= $page->slug() ?>">

  <header class="title<?php if($page->coverImages()->isNotEmpty()): ?> title--image<?php endif ?>">
    <div class="title__headline">
      <?php if($page->headline()->isNotEmpty()): ?>
        <h1><?= $page->headline() ?></h1>
      <?php else: ?>
        <h1><?= $page->title() ?></h1>
      <?php endif ?>
    </div>
    <?php if($page->coverImages()->toFiles()->count() > 0): ?>
      <div class="title__images title__images--setof<?= $page->coverImages()->toFiles()->count() ?>">
        <?php foreach($page->coverImages()->toFiles() as $image): ?>
          <figure>
            <img src="<?= $image->thumb(['width' => 1280, 'height' => 640, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            <?php if($image->cite()->isNotEmpty()): ?>
              <cite><?= $image->cite()->html() ?></cite>
            <?php endif ?>
          </figure>
        <?php endforeach ?>
      </div>
    <?php endif ?>
  </header>

  <section class="page-main__content">

    <ul class="list-events">
      <?php foreach($kirby->collection('events') as $item): ?>
        <li class="list-events__item">
          <header>
            <h3>
            <time>
              <?php if($item->dateText()->isNotEmpty()): ?>
                <?= $item->dateText()->html() ?>
              <?php else: ?>
                <?= $item->dateStart()->toDate('d.m.Y') ?><?php e($item->dateEnd()->isNotEmpty(), ' – ' . $item->dateEnd()->toDate('d.m.Y')) ?>
              <?php endif ?>
            </time>
          </h3>
          </header>
          <?= $item->text()->kt() ?>
        </article>
      <?php endforeach ?>
    </ul>

  </section>

</main>

<?php snippet('footer') ?>
