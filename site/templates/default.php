<?php snippet('header') ?>

<main class="page-main" id="<?= $page->slug() ?>">

  <header class="title<?php if($page->coverImages()->isNotEmpty()): ?> title--image<?php endif ?>">
    <div class="title__headline">
      <?php if($page->headline()->isNotEmpty()): ?>
        <h1><?= $page->headline() ?></h1>
      <?php else: ?>
        <h1><?= $page->title() ?></h1>
      <?php endif ?>
    </div>
    <?php if($page->coverImages()->toFiles()->count() > 0): ?>
      <div class="title__images title__images--setof<?= $page->coverImages()->toFiles()->count() ?>">
        <?php foreach($page->coverImages()->toFiles() as $image): ?>
          <figure>
            <img src="<?= $image->thumb(['width' => 1280, 'height' => 640, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            <?php if($image->cite()->isNotEmpty()): ?>
              <cite><?= $image->cite()->html() ?></cite>
            <?php endif ?>
          </figure>
        <?php endforeach ?>
      </div>
    <?php endif ?>
  </header>

  <div class="page-main__content">
    <div class="text">
      <div class="text__main">
        <?= $page->text()->kirbytext() ?>
      </div>
      <?php if($page->testimonialImage()->isNotEmpty()): ?>
        <aside class="text__aside">
          <div class="testimonial">
            <div class="testimonial__image">
              <img src="<?= $page->testimonialImage()->toFile()->thumb(['width' => 640, 'height' => 640, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $image->title() ?>">
            </div>
            <div class="testimonial__quote">
              <blockquote>
                <?= $page->testimonialQuote()->kt() ?>
                <cite><?= $page->testimonialCite()->html() ?></cite>
              </blockquote>
            </div>
          </div>
        </aside>
      <?php endif ?>
    </div>
  </div>

  <?php if($page->sidenote()->isNotEmpty()): ?>
    <aside class="page-main__content page-main__content--aside">
      <div class="text">
        <div class="text__main">
          <?= $page->sidenote()->kirbytext() ?>
        </div>
      </div>
    </aside>
  <?php endif ?>

</main>

<?php snippet('footer') ?>
