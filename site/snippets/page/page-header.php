<header class="page-header">
  <button class="page-header__menu-btn trigger-menu" title="Das Menü der Webseite öffnen">
    <span class="page-header__menu-btn__text">Menü</span>
    <?php snippet('svg/icons/menu') ?>
  </button>
  <?php if($page->isHomePage()): ?>
    <div class="page-header__logo">
      <?php snippet('svg/logo') ?>
    </div>
  <?php else: ?>
    <a class="page-header__logo" href="<?= $site->url() ?>" title="Die <?= $site->title()->html() ?> Startseite anzeigen">
      <?php snippet('svg/logo') ?>
    </a>
  <?php endif ?>
</header>
