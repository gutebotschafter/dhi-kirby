<footer class="page-footer">
  <div class="page-footer__icon">
    <h2><?php snippet('svg/icon') ?></h2>
  </div>
  <div class="page-footer__wrapper">
    <section class="page-footer__partners">
      <h3>Unsere Partner</h3>
      <?php foreach($site->partners()->toStructure() as $item): ?>
        <a class="partner" href="<?= $item->website() ?>" aria-label="<?= $item->title() ?>">
          <img src="<?= $item->logo()->toFile()->thumb(['grayscale' => true, 'quality' => 90])->url() ?>" alt="<?= $item->title() ?>">
        </a>
      <?php endforeach ?>
      <?php if($site->donateCta()->isNotEmpty()): ?>
        <h3><?= $site->donateCta()->html() ?></h3>
      <?php endif ?>
      <?php if($site->donateAccounts()->toStructure()->count() > 0): ?>
        <ul class="list-accounts">
          <?php foreach($site->donateAccounts()->toStructure() as $item): ?>
            <li class="list-accounts__item">
              <strong>Spendenkonto: <?= $item->name()->html() ?></strong>
              <br><?= $item->location()->html() ?>
              <br>IBAN: <?= $item->iban()->html() ?>
              <br>BIC: <?= $item->bic()->html() ?>
            </li>
          <?php endforeach ?>
        </ul>
      <?php endif ?>
    </section>
    <section class="page-footer__contact">
      <h3>Kontakt</h3>
      <p>
        <strong><?= $site->author() ?></strong>
      </p>
      <?php if($site->address()->isNotEmpty()): ?>
        <p>
          <address><?= $site->address()->kti() ?></address>
        </p>
      <?php endif ?>
      <p>
        Tel: <a href="tel: <?= $site->phone() ?>" title="<?= $site->author() ?> anrufen"><?= $site->phone()->html() ?></a>
        <?php if($site->fax()->isNotEmpty()): ?>
          <br>Fax: <a href="tel: <?= $site->fax() ?>" title="<?= $site->author() ?> ein Fax schreiben"><?= $site->fax()->html() ?></a>
        <?php endif ?>
      </p>
      <p>
        <a href="mailto: <?= $site->email() ?>" title="<?= $site->author() ?> eine E-Mail schreiben"><?= $site->email()->htm() ?></a>
      </p>
      <p>
        <em><?= $site->description()->html() ?></em>
      </p>
    </section>
    <nav class="page-footer__meta">
      <?php foreach($site->menuMeta()->toPages() as $item): ?>
        <span class="page-footer__meta__item" >
          <a href="<?= $item->url() ?>" title="Die Seite <?= $item->title() ?> anzeigen"><?= $item->title() ?></a>
        </span>
      <?php endforeach ?>
      <span class="page-footer__meta__item" >
        <a href="#page" title="Zum Anfang der Seite springen">Zurück zum Seitenanfang</a>
      </span>
    </nav>
  </div>
</footer>
