<nav class="page-menu">
  <?php if($site->menuMain()->isNotEmpty()): ?>
    <div class="page-menu__pages">
      <?php foreach($site->menuMain()->toPages() as $item): ?>
        <span class="page-menu__pages__item" >
          <a href="<?= $item->url() ?>" title="Die Seite <?= $item->title() ?> anzeigen"><?= $item->title() ?></a>
        </span>
      <?php endforeach ?>
    </div>
  <?php endif ?>
  <div class="page-menu__icons">
    <span class="page-menu__icons__item">
      <a href="<?= $site->url() ?>" aria-label="Die <?= $site->title()->html() ?> Startseite anzeigen"><?php snippet('svg/icons/home') ?></a>
    </span>
    <span class="page-menu__icons__item">
      <a href="tel: <?= $site->phone() ?>" aria-label="<?= $site->author() ?> anrufen"><?php snippet('svg/icons/phone-call') ?></a>
    </span>
    <span class="page-menu__icons__item">
      <a href="mailto: <?= $site->email() ?>" aria-label="<?= $site->author() ?> eine E-Mail schreiben"><?php snippet('svg/icons/mail') ?></a>
    </span>
  </div>
  <button class="page-menu__menu-btn trigger-menu" title="Das Menü der Webseite schließen">
    <span class="page-menu__menu-btn__text">Zurück</span>
    <?php snippet('svg/icons/x') ?>
  </button>
  <div class="page-menu__overlay"></div>
</nav>
