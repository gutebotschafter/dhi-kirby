<!DOCTYPE html>

<html lang="de">

  <head>

    <meta charset="utf-8">
    <?php if($site->noIndex() == 'true' or $page->noIndex() == 'true'): ?>
      <meta name="robots" content="noindex, nofollow">
    <?php else: ?>
      <meta name="robots" content="index, follow">
    <?php endif ?>

    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, shrink-to-fit=no">

    <?php if($page->metaTitle()->isNotEmpty()): ?>
      <title><?= $page->metaTitle()->html() ?></title>
    <?php elseif($site->metaTitle()->isNotEmpty()): ?>
      <title><?= $site->metaTitle()->html() ?></title>
    <?php elseif($page->isHomePage()): ?>
      <title><?= $site->title()->html() ?> – <?= $site->claim()->html() ?></title>
    <?php else: ?>
      <title><?= $page->title()->html() ?> – <?= $site->title()->html() ?></title>
    <?php endif ?>

    <?php if($page->metaDescription()->isNotEmpty()): ?>
      <meta name="description" content="<?= $page->metaDescription()->html() ?>">
    <?php elseif($site->metaDescription()->isNotEmpty()): ?>
      <meta name="description" content="<?= $site->metaDescription()->html() ?>">
    <?php endif ?>

    <meta name="author" content="<?= $site->author()->html() ?>">

    <link rel="shortcut icon" href="<?= url('assets/images/favicon.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="<?= url('assets/images/appicon.png') ?>">

    <meta property="og:image" content="<?= url('assets/images/socialicon.png') ?>">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">

    <meta name="format-detection" content="telephone=no">

    <?= inlineRenderer('/assets/criticals.bundle.css', 'style') ?>
    <link rel="stylesheet" href="/assets/app.bundle.css">

    <?= $site->scriptsHead() ?>

  </head>

  <body class="page page--<?= $page->template() ?>" id="page">

    <?= $site->scriptsBody() ?>

    <?php snippet('page/page-menu') ?>

    <?php snippet('page/page-header') ?>
