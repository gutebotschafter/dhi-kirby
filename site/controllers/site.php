<?php

return function ($site, $pages, $page) {

  $pageEvents = $site->index()->filterBy('template', 'events')->first();
  $pageNews = $site->index()->filterBy('template', 'news')->first();
  $pageProjects = $site->index()->filterBy('template', 'projects')->first();


  return [
    'pageEvents'    => $pageEvents,
    'pageNews'      => $pageNews,
    'pageProjects'  => $pageProjects
  ];

};
