<?php

return function ($site) {
    return $site->index()->filterBy('template', 'event')->filter(function ($child) {
      return $child->dateEnd()->toDate() > time() or $child->dateStart()->toDate() > time();
    });
};
