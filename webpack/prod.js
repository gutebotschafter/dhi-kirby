const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./common');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    plugins: [
        new UglifyJsPlugin({
            sourceMap: true,
            parallel: true
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
    ]
});
