const webpack = require('webpack');
const merge = require('webpack-merge');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

const common = require('./common');

module.exports = merge(common, {
    devServer: {
        hot: true,
        historyApiFallback: true,
        host: '0.0.0.0',
        port: 8080,
        stats: {
            colors: true
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*'
        },
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        }
    },
    watchOptions: {
        poll: true
    },
    mode: 'development',
    devtool: 'cheap-eval-module-source-map',
    plugins: [
        new ProgressBarPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]
});
